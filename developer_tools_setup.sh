#! /bin/sh

echo "## Developer Tools Setup"
echo ''
# install zsh
# install oh-my-zsh
# https://gist.github.com/dogrocker/1efb8fd9427779c827058f873b94df95
`sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
# install plugins
"sudo apt-get install -y zsh-autosuggestions"
"git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting"
"cp developer_tools/zsh/.zshrc ~/.zshrc"
"./developer_tools/ubuntu_docker.sh"
