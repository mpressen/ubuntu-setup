# ubuntu-setup

shell scripts for setting up ubuntu 18.04 LTS

## pre-requisites
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git
```


## init

```
git clone https://gitlab.com/mpressen/ubuntu-setup.git
cd ubuntu_setup
# give executable rights for all sh files recursively
find . -type f -name "*.sh" -exec chmod +x {} \;
```